#############################################################################
# Copyright (c) 2019 Accenture.
# All rights reserved.
#
# This source code and any compilation or derivative thereof is the
# proprietary information of Accenture and is confidential in nature.
#
# -----------------------------------------------------
# http://www.accenture.com
# -----------------------------------------------------
#############################################################################

"""This module is to print the current temperature in Delft city"""

##############################################################################
# Import modules
##############################################################################
import requests
from bs4 import BeautifulSoup

"""Getting clientraw.txt file since temperature value not found in homepage html """

URL = 'https://weerindelft.nl/clientraw.txt'
R = requests.get(URL)

SOUP = BeautifulSoup(R.content, 'html.parser')

"""Found the 4th value as actual temp by debugging javascript"""

TEMP = str(SOUP).split()[4]
INT = (round(float(TEMP)))

print(str("The current temperature in Delft city is ")+str(INT)+str(" degrees Celsius"))
