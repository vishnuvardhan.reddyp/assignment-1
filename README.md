* Author:         Vishnuvardhan Reddy Pocha
* Date:           03/12/2019
* Purpose:        To display the current temperature in Delft city from https://weerindelft.nl/
* Instructions:   To run job manually by clicking Run Pipeline button here: https://gitlab.com/vishnuvardhan.reddyp/assignment-1/pipelines
