#############################################################################
# Copyright (c) 2019 Accenture.
# All rights reserved.
#
# This source code and any compilation or derivative thereof is the
# proprietary information of Accenture and is confidential in nature.
#
# -----------------------------------------------------
# http://www.accenture.com
# -----------------------------------------------------
#############################################################################

#This module is calling by gitlab ci yml after creating the docker container

##############################################################################
# Import modules
##############################################################################

#Python module in the new created docker container
FROM python:3
#Installing required modules to run the python file
RUN pip install \
  requests \
  bs4
ADD how_warm_is_het_in_delft.py /
CMD [ "python" , "./how_warm_is_het_in_delft.py" ]